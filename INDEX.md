# DOjS

# DOjS

The repository for this project is only a duplicate. It is actively
maintained by it's developer and a more recent version may exist at the
original developers website.

If you wish to contribute or submit any bug reports for this project, please
visit the developers website at https://github.com/SuperIlu/DOjS

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## DOJS.LSM

<table>
<tr><td>title</td><td>DOjS</td></tr>
<tr><td>version</td><td>1.13.0</td></tr>
<tr><td>entered&nbsp;date</td><td>2021-01-22</td></tr>
<tr><td>description</td><td>A DOS JavaScript Canvas with sound</td></tr>
<tr><td>summary</td><td>DOjS is a JavaScript-able canvas with WAV and MIDI sound support. DOjS runs in Dosbox and on real hardware or a virtual machine with MS-DOS, FreeDOS or any DOS based Windows like Windows 95/98/ME. If you run it on real hardware you need at least a 80386 with 4MB. I recommend a Pentium class machine (&gt;= 100MHz) with at least 32MB RAM.</td></tr>
<tr><td>keywords</td><td>java, JavaScript</td></tr>
<tr><td>author</td><td>SuperIlu</td></tr>
<tr><td>maintained&nbsp;by</td><td>SuperIlu</td></tr>
<tr><td>primary&nbsp;site</td><td>https://github.com/SuperIlu/DOjS</td></tr>
<tr><td>original&nbsp;site</td><td>https://github.com/SuperIlu/DOjS</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[Various Licenses, See LICENSE file](LICENSE)</td></tr>
</table>
